import 'package:flutter/material.dart';

// Page routing status as enum
enum AppRouteStatus { home, mapping, settings, status, video,editTable }

class AppRouteStatusProvider with ChangeNotifier {
  
  late AppRouteStatus _appRouteStatus;

  AppRouteStatus get appRouteStatus => _appRouteStatus;

  set appRouteStatus(AppRouteStatus inStatus) {
    _appRouteStatus = inStatus;
    notifyListeners();
  }
}