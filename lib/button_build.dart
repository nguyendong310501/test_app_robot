import 'package:flutter/material.dart';
import 'button_color_provider.dart';
import 'package:provider/provider.dart';
import 'ros_provider.dart';

class Button extends StatefulWidget{
  final String? tableNumber;
  final int? index;
  final Function() onEditingComplete;
  const Button({super.key, required this.tableNumber, required this.onEditingComplete, required this.index});

  @override
  State<Button> createState() => _ButtonState();
}

class _ButtonState extends State<Button> {

  @override
  Widget build(BuildContext context) {
    return  Consumer2<ButtonColorProvider,RosProvider>(
      builder: (context, buttonColor, ros, child) {
        return ElevatedButton(
          style: ButtonStyle(
            shape: MaterialStateProperty.all<RoundedRectangleBorder>(
              RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(18.0),
              )
            ),
            backgroundColor:  MaterialStateProperty.all<Color>((widget.index == buttonColor.buttonColorIndex ? Color.fromARGB(255, 0, 255, 0) : Color.fromARGB(255, 95, 223, 133))) // Example: Change background color when pressed
          ),
          onPressed: () {
            buttonColor.buttonColorIndex = widget.index??0;
            Map<String, dynamic> json = {"data": widget.index.toString()};
            ros.commonMessage.publish(json);
          },
          onLongPress: widget.onEditingComplete,
          child: Text(
            widget.tableNumber??'',
            style: const TextStyle(
              fontSize: 40,
              color: Colors.black,
              ),
          ),
        );
      }
    );
  }
  
  @override
  void dispose() {
    super.dispose();
  }
}
