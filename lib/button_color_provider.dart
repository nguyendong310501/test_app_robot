import 'package:flutter/material.dart';

class ButtonColorProvider with ChangeNotifier {
  int _buttonColorIndex = -1;

  int get buttonColorIndex => _buttonColorIndex;

  set buttonColorIndex(int index) {
    _buttonColorIndex = index;
    notifyListeners();
  }
}