import 'package:flutter/material.dart';
import 'app_state_provider.dart';
import 'package:provider/provider.dart';

const kTitle = 'MENU';

class DrawerMenu extends StatelessWidget {
  const DrawerMenu({super.key});

  @override
  Widget build(BuildContext context) {
    return Drawer(
      child: ListView(
        padding: EdgeInsets.zero,
        children: <Widget>[
          const DrawerHeader(
            decoration: BoxDecoration(
              color: Colors.teal,
            ),
            child: Center(
              child: Text(
                kTitle,
                style: TextStyle(
                  fontSize: 24,
                  color: Colors.white,
                ),
              ),
            ),
          ),
          getListTile('Home', onTap: () {
            final app = Provider.of<AppRouteStatusProvider>(context, listen: false);
            app.appRouteStatus = AppRouteStatus.home;
            Navigator.pushReplacementNamed(context, '/');
          }),
          getLine(),
          getListTile('Mapping', onTap: () {
            final app = Provider.of<AppRouteStatusProvider>(context, listen: false);
            app.appRouteStatus = AppRouteStatus.mapping;
            Navigator.pushReplacementNamed(context, '/mapping');
          }),
          getLine(),
          getListTile('Video', onTap: () {
            final app = Provider.of<AppRouteStatusProvider>(context, listen: false);
            app.appRouteStatus = AppRouteStatus.video;
            Navigator.pushReplacementNamed(context, '/video');
          }),
          getLine(),
          getListTile('Settings', onTap: () {
            final app = Provider.of<AppRouteStatusProvider>(context, listen: false);
            app.appRouteStatus = AppRouteStatus.settings;
            Navigator.pushReplacementNamed(context, '/settings');
          }),
          getLine(),
          getListTile('Edit Table', onTap: () {
            final app = Provider.of<AppRouteStatusProvider>(context, listen: false);
            app.appRouteStatus = AppRouteStatus.editTable;
            Navigator.pushReplacementNamed(context, '/editTable');
          }),
        ],
      ),
    );
  }
  Widget getLine() {
    return SizedBox(
      height: 0.5,
      child: Container(
        color: Colors.grey,
      ),
    );
  }

  Widget getListTile(title,{void Function()? onTap}) {
    return ListTile(
      title: Text(title),
      onTap: onTap,
    );
  }
}