import 'dart:async';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:refactor_flutter/ros_provider.dart';
import 'button_build.dart';
import 'button_color_provider.dart';
import 'drawer_menu.dart';
import 'robot_settings_provider.dart';
import 'package:battery_indicator/battery_indicator.dart';
import 'package:roslibdart/roslibdart.dart';

class Home extends StatefulWidget {
  const Home({super.key});
  @override
  State<Home> createState() => _Home();
}

class _Home extends State<Home> {
  _Home();
  String text = "";

  @override
  void initState() {
    super.initState();
  }

  @override
  void dispose() {
    super.dispose();
  }

  Stream<Status> update() async* {}

  void _showDialog(TextEditingController controller, int index) {
    showDialog(
      context: context,
      builder: (BuildContext context) {
        return AlertDialog(
         scrollable: true,
          content: Column(
            mainAxisSize: MainAxisSize.min,
            children: <Widget>[
              TextFormField(
                controller: controller,
                decoration: const InputDecoration(hintText: 'Enter text', hintStyle: TextStyle(fontSize: 14.0,)),
                onChanged: (String text) {
                  Provider.of<SettingsProvider>(context, listen: false)
                      .updateText(text);
                },
                validator: (text) {
                  if (text == null || text.isEmpty) {
                    return 'Please enter some text.';
                  }
                  if (text.isEmpty || text.length > 20) {
                    return 'Text must be between 1 and 20 characters.';
                  }
                  return null;
                },
                autovalidateMode: AutovalidateMode.onUserInteraction,
              ),
              const SizedBox(height: 20),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  TextButton(
                    onPressed: () {
                      controller.clear();
                      Navigator.of(context).pop();
                    },
                    style: ButtonStyle(
                      backgroundColor: MaterialStateProperty.all<Color>(
                        const Color.fromARGB(255, 95, 223, 133), // color for button cancel
                      ),
                    ),
                    child: const Text(
                      'Cancel',
                      style: TextStyle(
                        color: Colors.white, // while color for button cancel
                      ),
                    ),
                  ),
                  TextButton(
                    onPressed: () {                     
                        Provider.of<SettingsProvider>(context, listen: false)
                            .updateList(index);
                        controller.clear();
                        Navigator.of(context).pop();
                    },
                    style: ButtonStyle(
                      backgroundColor: MaterialStateProperty.all<Color>(
                        const Color.fromARGB(255, 95, 223, 133), // color green for button save
                      ),
                    ),
                    child: const Text(
                      'Save',
                      style: TextStyle(
                        color: Colors.white, // color while for button save
                      ),
                    ),
                  ),
                ],
              ),
            ],
          ),
        );
      },
    );
  }

  @override
  Widget build(BuildContext context) {

    return Scaffold(
      appBar: AppBar(
        title: Center(
          child: Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Consumer<RosProvider>(builder: (context, ros, child) {
                return StreamBuilder<Status>(
                    stream: ros.ros.statusStream,
                    initialData: ros.ros.status,
                    builder: (context, snapshot) {
                      if (snapshot.hasData) {
                        final currentStatus = snapshot.data;
                        switch (currentStatus) {
                          case Status.none:
                            text = 'Connect';
                            break;
                          case Status.closed:
                            text = 'Connect';
                            break;
                          case Status.connecting:
                            text = 'Connecting';
                            break;
                          case Status.connected:
                            text = 'Connected';
                            break;
                          default:
                            text = '';
                        }
                      }
                      return ElevatedButton(
                        style: ButtonStyle(
                            backgroundColor:
                            MaterialStateProperty.all<Color>(
                                (snapshot.data == Status.connected) ||
                                    (snapshot.data ==
                                        Status.connecting)
                                    ? const Color.fromARGB(255, 0, 255, 0)
                                    : const Color.fromARGB(255, 230, 83, 39))),
                        onPressed: () {
                          if (snapshot.hasError) {
                            // TODO: Error handling
                          } else {
                            switch (snapshot.connectionState) {
                              case ConnectionState.none:
                                break;
                              case ConnectionState.waiting:
                                break;
                              case ConnectionState.active:
                                break;
                              case ConnectionState.done:
                                break;
                            }
                          }
                          if ((ros.ros.status == Status.closed) ||
                              (ros.ros.status == Status.none)) {
                            ros.connectRos();
                          }
                        },
                        child: Text(
                          text,
                          style: const TextStyle(
                            fontStyle: FontStyle.italic,
                            fontSize: 30,
                            color: Colors.black,
                          ),
                        ),
                      );
                    });
              })
            ],
          ),
        ),
        backgroundColor: const Color.fromARGB(255, 95, 223, 133),
      ),
      body: SingleChildScrollView(
        child: Column(
          children: <Widget>[
            SizedBox(
              height: MediaQuery.of(context).size.height *
                  0.6, // constrain height
              child: ListView(
                children: <Widget>[
                  Consumer<SettingsProvider>(
                    builder: (context, setting, child) {
                      return GridView.builder(
                        primary: true,
                        padding: const EdgeInsets.all(20),
                        physics: const NeverScrollableScrollPhysics(),
                        shrinkWrap: true,
                        gridDelegate:
                        const SliverGridDelegateWithFixedCrossAxisCount(
                          crossAxisCount: 3,
                          crossAxisSpacing: 20.0,
                          mainAxisSpacing: 20.0,
                          childAspectRatio: 3.0,
                        ),
                        itemCount: setting.numberOfTable,
                        itemBuilder: (context, index) {
                          final String buttonNumber = setting.listTable[index].customerName??'';
                          return Button(
                            onEditingComplete: () {
                               Provider.of<SettingsProvider>(context, listen: false)
                        .updateTextForm(buttonNumber);
                              _showDialog(setting.inputText, index);
                             
                                
                            },
                            tableNumber: buttonNumber,
                            index: (index + 1),
                          );
                        },
                      );
                    },
                  ),
                ],
              ),
            ),
            SizedBox(
              height: MediaQuery.of(context).size.height * 0.05,
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.start,
              children: [
                SizedBox(
                  width: MediaQuery.of(context).size.width * 0.05,
                ),
                Column(
                  children: [
                    Consumer<RosProvider>(builder: (context, ros, child) {
                      return BatteryIndicator(
                        batteryFromPhone: false,
                        batteryLevel:
                        int.parse(ros.infoMsgReceived['data']!),
                        style: BatteryIndicatorStyle.skeumorphism,
                        colorful: true,
                        showPercentNum: true,
                        showPercentSlide: true,
                        size: MediaQuery.of(context).size.height * 0.05,
                      );
                    }),
                    SizedBox(
                        height: MediaQuery.of(context).size.height * 0.08,
                        width: MediaQuery.of(context).size.width * 0.08,
                        child: const ImageIcon(
                            color: Colors.black,
                            AssetImage("assets/robot.png")))
                  ],
                ),
                SizedBox(
                  width: MediaQuery.of(context).size.width * 0.1,
                ),
                Consumer2<ButtonColorProvider, RosProvider>(
                    builder: (context, buttonColor, ros, child) {
                      return SizedBox(
                        height: MediaQuery.of(context).size.height * 0.15,
                        width: MediaQuery.of(context).size.width * 0.3,
                        child: ElevatedButton(
                            style: ButtonStyle(
                                shape: MaterialStateProperty.all<
                                    RoundedRectangleBorder>(
                                    RoundedRectangleBorder(
                                      borderRadius: BorderRadius.circular(18.0),
                                    )),
                                backgroundColor:
                                MaterialStateProperty.all<Color>(
                                    (0 == buttonColor.buttonColorIndex
                                        ? const Color.fromARGB(255, 0, 255, 0)
                                        : const Color.fromARGB(255, 95, 223,
                                        133))) // Example: Change background color when pressed
                            ),
                            onPressed: () {
                              buttonColor.buttonColorIndex = 0;
                              Map<String, dynamic> json = {"data": "0"};
                              ros.commonMessage.publish(json);
                            },
                            child: Icon(Icons.home,
                                size: MediaQuery.of(context).size.height * 0.1)),
                      );
                    }),
                SizedBox(
                  width: MediaQuery.of(context).size.width * 0.1,
                ),
                SizedBox(
                    height: MediaQuery.of(context).size.height * 0.2,
                    width: MediaQuery.of(context).size.width * 0.35,
                    child: Consumer<RosProvider>(builder: (context, ros, child) {
                      return ros.imageData != null
                          ? Image.memory(
                        ros.imageData!,
                        fit: BoxFit.contain,
                      )
                          : const Text('No image received');
                    })),
              ],
            ),
          ],
        ),
      ),
      drawer: const DrawerMenu(),
    );
  }
}
