import 'package:get_it/get_it.dart';
import 'package:refactor_flutter/shared_prefs.dart';

GetIt locator = GetIt.instance;

void setUpInjector() {
  locator.registerSingleton(() => SharedPrefs());  //Register Shared preferences
}

