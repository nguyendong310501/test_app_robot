import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:refactor_flutter/locator.dart';
import 'package:refactor_flutter/robot_edit_table.dart';
import 'package:refactor_flutter/shared_prefs.dart';
import 'ros_provider.dart';
import 'home.dart';
import 'mapping.dart';
import 'robot_settings.dart';
import 'robot_settings_provider.dart';
import 'button_color_provider.dart';
import 'robot_status.dart';
import 'robot_video_streaming.dart';
import 'app_state_provider.dart';

void main() {
  setUpInjector();
  runApp(
    MultiProvider(
      providers: [
        ChangeNotifierProvider(create: (_) => RosProvider()),
        ChangeNotifierProvider(create: (_) => SettingsProvider()),
        ChangeNotifierProvider(create: (_) => ButtonColorProvider()),
        ChangeNotifierProvider(create: (_) => AppRouteStatusProvider()),
      ],
      child: const MainApp(),
    ),
  );
}

class MainApp extends StatefulWidget {
  const MainApp({super.key});

  @override
  State<MainApp> createState() => _MainAppState();
}

class _MainAppState extends State<MainApp> {
  @override
  void initState() {
    final ros = Provider.of<RosProvider>(context, listen: false);
    ros.initRos();
    ros.createRosTopic();
    super.initState();
  }
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
        debugShowCheckedModeBanner: false,
        initialRoute: '/',
        routes: {
          '/': (context) => const Home(),
          '/mapping': (context) => Mapping(),
          '/settings': (context) => const RobotSetting(),
          '/status': (context) => const RobotStatus(),
          '/video': (context) =>  RobotVideo(),
          '/editTable': (context) =>const RobotEditTable()
        },
    );
  }
}