import 'package:flutter/material.dart';
import 'drawer_menu.dart';
import 'package:provider/provider.dart';
import 'robot_settings_provider.dart';

class RobotEditTable extends StatelessWidget {
  const RobotEditTable({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(
          'Robot Edit Table',
          style: TextStyle(
            fontSize: MediaQuery.of(context).size.height * 0.05,
          ),
        ),
        toolbarHeight: MediaQuery.of(context).size.height * 0.1,
      ),
      body: Consumer<SettingsProvider>(builder: (context, setting, child) {
        return ListView(
          padding: const EdgeInsets.all(16),
          children: [
            PaginatedDataTable(
                showCheckboxColumn: false,
                header: Text('Edit Table'),
                rowsPerPage: 8,
                columns: const [
                  DataColumn(label: Text('ID')),
                  DataColumn(label: Text('Default Name')),
                  DataColumn(label: Text('Customer Name')),
                  DataColumn(label: Text('Action')),
                ],
                source:
                    _DataSource(context, setting.listTable, setting.inputText)),
            Container(
              child: Row(mainAxisAlignment: MainAxisAlignment.end, children: [
                TextButton(
                    style: ButtonStyle(
                      backgroundColor: MaterialStateProperty.all<Color>(
                        Color.fromARGB(
                            255, 82, 184, 253), // color for button cancel
                      ),
                    ),
                    onPressed: () {
                      Provider.of<SettingsProvider>(context, listen: false)
                        .resetListTable();
                    },
                    child:
                        Text('Reset', style: TextStyle(color: Colors.white))),
                const SizedBox(
                  width: 10,
                ),
                TextButton(
                    style: ButtonStyle(
                      backgroundColor: MaterialStateProperty.all<Color>(!setting.isDissible?
                        const Color.fromARGB(
                            255, 95, 223, 133):Color.fromARGB(255, 193, 193, 193)// color for button cancel
                      ),
                    ),
                    onPressed: () {
                      if(!setting.isDissible){
                         Provider.of<SettingsProvider>(context, listen: false)
                        .saveToSharedPreferences();
                      }                       
                    },
                    child: Text(
                      'Save',
                      style: TextStyle(color: !setting.isDissible? Colors.white:Colors.black),
                    )),
              ]),
            )
          ],
        );
      }),
      drawer: DrawerMenu(),
    );
  }
}

class _DataSource extends DataTableSource {
  _DataSource(this.context, this._rows, this.controller);

  final BuildContext context;
  final List<EditTableModel> _rows;
  final TextEditingController controller;

  int _selectedCount = 0;

  @override
  DataRow? getRow(int index) {
    assert(index >= 0);
    if (index >= _rows.length) return null;
    final row = _rows[index];
    return DataRow.byIndex(
      index: index,
      selected: row.selected,
      onSelectChanged: (value) {
       
      },
      cells: [
        DataCell(Text(index.toString())),
        DataCell(Text(row.defaultName!)),
        DataCell(row.selected
            ? Container(
                child: TextFormField(
                  controller: controller,
                  decoration: const InputDecoration(
                      hintText: 'Enter text',
                      hintStyle: TextStyle(
                        fontSize: 14.0,
                      ),
                      border: InputBorder.none, ),
                  onChanged: (String text) {
                    Provider.of<SettingsProvider>(context, listen: false)
                        .updateText(text);
                  },
                  validator: (text) {
                    if (text == null || text.isEmpty) {
                      return 'Please enter some text.';
                    }
                    if (text.isEmpty || text.length > 20) {
                      return 'Text must be between 1 and 20 characters.';
                    }
                    return null;
                  },
                  autovalidateMode: AutovalidateMode.onUserInteraction,
                ),
              )
            : Text(row.customerName!)),
        DataCell(Container(
          child: Row(
            children: [
              TextButton(
                  onPressed: () {
                    Provider.of<SettingsProvider>(context, listen: false)
                        .selectEditTable(index);
                        Provider.of<SettingsProvider>(context, listen: false)
                        .updateTextForm(row.customerName);
                        Provider.of<SettingsProvider>(context, listen: false)
                        .updateSelectIndex(index);
                  },
                  child: Text('Edit'))
            ],
          ),
        )),
      ],
    );
  }

  @override
  int get rowCount => _rows.length;

  @override
  bool get isRowCountApproximate => false;

  @override
  int get selectedRowCount => _selectedCount;
}
