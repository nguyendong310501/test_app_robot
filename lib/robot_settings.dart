import 'package:flutter/material.dart';
import 'drawer_menu.dart';
import 'package:settings_ui/settings_ui.dart';
import 'package:numberpicker/numberpicker.dart';
import 'package:provider/provider.dart';
import 'robot_settings_provider.dart';
import 'ros_provider.dart';

class RobotSetting extends StatelessWidget {
   const RobotSetting({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(
          'Robot Settings',
          style: TextStyle(
            fontSize: MediaQuery.of(context).size.height * 0.05,
          ),
        ),
        toolbarHeight: MediaQuery.of(context).size.height * 0.1,
      ),
      body: Column(
        children: <Widget>[
          Container(
            height: MediaQuery.of(context).size.height*0.6,
            color: Colors.green,
            child: Consumer2<SettingsProvider,RosProvider>(
              builder: (context, setting, rosprovider , child) {
              return SettingsList(
                platform: DevicePlatform.android,
                sections: [
                    SettingsSection(
                      title: const Text('ModeList'),
                      tiles: <SettingsTile>[
                        SettingsTile.switchTile(
                          initialValue: setting.isOperating,
                          onToggle: (value) {
                            setting.isOperating = value;
                            if(setting.isOperating == true) {
                              Map<String, dynamic> json = {"data": "ScanMode"};
                              rosprovider.commonMessage.publish(json);
                            }
                          },
                          leading: const Icon(Icons.navigation_outlined),
                          title: const Text('Discovery mode'),
                        ),
                        SettingsTile.switchTile(
                          initialValue: setting.isDiscovery,
                          onToggle: (value) {
                            setting.isDiscovery = value;
                            if(setting.isDiscovery == true) {
                              Map<String, dynamic> json = {"data": "NaviMode"};
                              rosprovider.commonMessage.publish(json);
                            }
                          },
                          leading: const Icon(Icons.agriculture),
                          title: const Text('Operation mode'),
                        ),
                      ],
                    ),
                  ],
                );
              }
            ),
          ),
          Container(
              alignment: Alignment.center,
              height: MediaQuery.of(context).size.height*0.1,
              color: const Color.fromARGB(255, 32, 168, 202),
              child: const Text("Choose number of tables below",
                textScaleFactor: 1.5,
              ),
          ),
          Consumer<SettingsProvider>(
            builder: (context, setting, child) {
              return NumberPicker(
                itemHeight: MediaQuery.of(context).size.height * 0.1,
                itemWidth: MediaQuery.of(context).size.height * 0.1,
                axis: Axis.horizontal,
                textStyle: const TextStyle(
                  color: Color.fromARGB(255, 35, 122, 194),
                  fontSize: 30,
                ),
                selectedTextStyle: const TextStyle(
                  color: Color.fromARGB(255, 35, 122, 194),
                  fontSize: 30,
                ),
                value: setting.numberOfTable,
                minValue: 1,
                maxValue: 50,
                step: 1,
                haptics: true,
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(16),
                  border: Border.all(color: Colors.black26),
                ),
                onChanged: (int value){
                    setting.numberOfTable = value;
                    setting.updateNumber(value);//
                },
              );
            }
          ),
        ],
      ),
      drawer: DrawerMenu(),
    );
  }
}