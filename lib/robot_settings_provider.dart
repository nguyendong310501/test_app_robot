import 'dart:convert';
import 'dart:math';

import 'package:flutter/material.dart';
import 'package:refactor_flutter/shared_prefs.dart';

class SettingsProvider with ChangeNotifier {
  TextEditingController inputText = TextEditingController();
  String? text;
  List<EditTableModel> _listDataTable = <EditTableModel>[];
  int selectIndex = 0;

  SharedPrefs sharedPrefs = SharedPrefs();

  int _numberOfTable = 9;
  bool _isOperating = false;
  bool _isDiscovery = false;

  SettingsProvider() {
    WidgetsBinding.instance.addPostFrameCallback((_) {
      onInit();
    });
  }

  void onInit() {
    getDataTableNumbers();
    getNumber();
  }

  set numberOfTable(newValue) {
    _numberOfTable = newValue;
    notifyListeners();
  }

  set isOperating(newValue) {
    _isOperating = newValue;
    if (_isDiscovery == true) {
      _isDiscovery = !newValue;
    }
    notifyListeners();
  }

  void selectEditTable(int index) {
    _listDataTable.forEach((element) {
      element.selected = false;
    });
    _listDataTable[index].selected = true;
    notifyListeners();
  }

  set isDiscovery(newValue) {
    _isDiscovery = newValue;
    if (_isOperating == true) {
      _isOperating = !newValue;
    }
    notifyListeners();
  }

   bool checkBtnSave(){
    for (var element in listTable) { if(element.selected==true){ return false;} }
    return true;
  }

  int get numberOfTable => _numberOfTable;

  bool get isOperating => _isOperating;

  bool get isDiscovery => _isDiscovery;

  List<EditTableModel> get listTable => _listDataTable;

  bool get isDissible =>checkBtnSave();

  void updateText(String? texts) {
    text = texts ?? '';
    notifyListeners();
  }

  void updateList(int? index) {
    _listDataTable[index ?? 0].customerName = text;
    List<String> jsonList =
        _listDataTable.map((model) => jsonEncode(model.toJson())).toList();
    sharedPrefs.tableNumbers = jsonList;
    notifyListeners();
  }

  void getNumber() async {
    await sharedPrefs.initialise();
    _numberOfTable = sharedPrefs.numbers ?? _numberOfTable;
    notifyListeners();
  }

  void updateNumber(int? number) {
    sharedPrefs.numbers = number ?? 0;
  }

  void updateSelectIndex(int? index) {
    selectIndex = index ?? 0;
    notifyListeners();
  }

  void updateTextForm(String? st) {
    inputText = TextEditingController(text: st ?? '');
    notifyListeners();
  }

  void resetListTable(){
    for (var element in _listDataTable) {element.customerName = element.defaultName; }
    notifyListeners();
  }

  Future<void> saveToSharedPreferences() async {
    await sharedPrefs.initialise();
    _listDataTable[selectIndex].customerName = text;
    for (var element in _listDataTable) {
      element.selected = false;
    }
    List<String> jsonList =
        _listDataTable.map((model) => jsonEncode(model.toJson())).toList();
    sharedPrefs.tableNumbers = jsonList;
    notifyListeners();
  }

  void getDataTableNumbers() async {
    await sharedPrefs.initialise();
    late List<String> lists = sharedPrefs.tableNumbers ?? <String>[];
    if (lists.isNotEmpty) {
      List<EditTableModel> convertedModels = lists.map((jsonString) {
        Map<String, dynamic> jsonMap = jsonDecode(jsonString);
        return EditTableModel.fromJson(jsonMap);
      }).toList();
      _listDataTable = convertedModels;
      notifyListeners();
    } else {
      _listDataTable = List.generate(
          50, (index) => EditTableModel('A${index + 1}', 'A${index + 1}'));
    }
  }
}

class EditTableModel {
  String? defaultName;
  String? customerName;
  bool selected = false;

  EditTableModel(this.customerName, this.defaultName);

  // Convert the model to a Map
  Map<String, dynamic> toJson() {
    return {
      'defaultName': defaultName,
      'customerName': customerName,
      'selected': selected,
    };
  }

  // Create a model from a Map
  factory EditTableModel.fromJson(Map<String, dynamic> json) {
    return EditTableModel(
      json['customerName'],
      json['defaultName'],
    )..selected = json['selected'] ?? false;
  }
}
