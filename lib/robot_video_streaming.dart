import 'package:flutter/material.dart';
import 'package:arrow_pad/arrow_pad.dart';
import 'package:provider/provider.dart';
import 'package:webview_flutter/webview_flutter.dart';
import 'package:numberpicker/numberpicker.dart';
import 'drawer_menu.dart';
import 'ros_provider.dart';

class RobotVideo extends StatelessWidget {
  RobotVideo({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: WebViewMapPage(),
    );
  }
}

class WebViewMapPage extends StatefulWidget {
  const WebViewMapPage({super.key});
  @override
  // ignore: library_private_types_in_public_api, no_logic_in_create_state
  _WebViewMapPage createState() => _WebViewMapPage();
}

class _WebViewMapPage extends State<WebViewMapPage> {
  late final WebViewController controller;

  var tableToSetGoal = 1;
  String url = "http://10.1.1.9:5000";
  // String url = "http://10.42.0.1:5000";

  void _onChanged(int val) {
    setState(() {
      tableToSetGoal = val;
    });
  }
  @override
  void initState() {
    super.initState();

    // #docregion webview_controller
    controller = WebViewController()
      ..enableZoom(false)
      ..setJavaScriptMode(JavaScriptMode.unrestricted)
      ..setBackgroundColor(Color.fromARGB(0, 253, 255, 255))
      ..setNavigationDelegate(
        NavigationDelegate(
          onProgress: (int progress) {
            // Update loading bar.
          },
          onPageStarted: (String url) {},
          onPageFinished: (String url) {},
          onWebResourceError: (WebResourceError error) {},
          onNavigationRequest: (NavigationRequest request) {
            //if (request.url.startsWith('http://10.13.13.10:8000/export/urdf.html')) {
            if (request.url.startsWith(url)) {
              return NavigationDecision.prevent;
            }
            return NavigationDecision.navigate;
          },
        ),
      )
      //..loadRequest(Uri.parse('http://10.42.0.1:8000/export/urdf.html'));
      //..loadRequest(Uri.parse('http://10.13.13.10:8000/export/urdf.html'));
      ..loadRequest(Uri.parse(url));
    // #enddocregion webview_controller
  }

  @override
  void dispose(){
      //...
      //print("dispose called");
      controller.loadRequest(Uri.parse('about:blank'));
      super.dispose();
      //...
  }

  // #docregion webview_widget
  @override
  Widget build(BuildContext context) {
    double height = MediaQuery.of(context).size.height;
    double width = MediaQuery.of(context).size.width;
    return Scaffold(
      appBar: AppBar(
      title: const Text("VideoStreaming"),
      actions: <Widget>[
          Row(
            children: [
              IconButton(
                icon: const Icon(Icons.refresh),
                iconSize: 35,
                onPressed: () {
                  // using currentState with question mark to ensure it's not null
                  controller.reload();
                },
              ),
              SizedBox(width: MediaQuery.of(context).size.height * 0.2)
            ],
          )
        ],
      ),
      //body: WebViewWidget(controller: controller),
      body:GestureDetector(
        // Disable webview scrolling by overiding this function
        onHorizontalDragUpdate: (updateDetails) {},
        child: Stack(
          children: <Widget>[
            SizedBox(
              height: MediaQuery.of(context).size.height * 0.9,
              width: MediaQuery.of(context).size.width * 0.9,
              child: WebViewWidget(controller: controller)
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.end,
              children: [
                Container(
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      SizedBox(
                        child: NumberPicker(
                          itemHeight: MediaQuery.of(context).size.height * 0.1,
                          itemWidth: MediaQuery.of(context).size.height * 0.1,
                          axis: Axis.horizontal,
                          textStyle: const TextStyle(
                            color: Color.fromARGB(255, 35, 122, 194),
                            fontSize: 30,
                          ),
                          selectedTextStyle: const TextStyle(
                            color: Color.fromARGB(255, 35, 122, 194),
                            fontSize: 30,
                          ),
                          value: tableToSetGoal,
                          minValue: 0,
                          maxValue: 50,
                          step: 1,
                          haptics: true,
                          decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(16),
                            border: Border.all(color: Colors.black26),
                          ),
                          onChanged: _onChanged,
                        ),
                      ),
                      SizedBox(height: MediaQuery.of(context).size.height * 0.02),
                      Transform.scale(
                        scale: 0.75,
                        child: Consumer<RosProvider>(
                          builder: (context, ros, child) {
                            return ElevatedButton(
                              style: ButtonStyle(
                                shape: MaterialStateProperty.all<RoundedRectangleBorder>(
                                  RoundedRectangleBorder(
                                    borderRadius: BorderRadius.circular(18.0),
                                  )
                                ),
                                backgroundColor:  MaterialStateProperty.all<Color>( Colors.blue) // Example: Change background color when pressed
                              ),
                              onPressed: () {
                                Map<String, dynamic> json = {"data": "$tableToSetGoal Setting"};
                                ros.commonMessage.publish(json);
                              },
                              child: const Text(
                                "SaveGoal",
                                style: TextStyle(
                                  fontSize: 35,
                                  color: Colors.white,
                                  ),
                              ),
                            );
                          }
                        ),
                      ),
                      SizedBox(height: MediaQuery.of(context).size.height * 0.02),
                      Transform.scale(
                        scale: 0.75,
                        child: Consumer<RosProvider>(
                          builder: (context, ros, child) {
                            return ElevatedButton(
                              style: ButtonStyle(
                                shape: MaterialStateProperty.all<RoundedRectangleBorder>(
                                  RoundedRectangleBorder(
                                    borderRadius: BorderRadius.circular(18.0),
                                  )
                                ),
                                backgroundColor:  MaterialStateProperty.all<Color>((true ? Color.fromARGB(255, 255, 0, 0) : Color.fromARGB(255, 95, 223, 133))) // Example: Change background color when pressed
                              ),
                              onPressed: () {
                                  ros.linear['x'] = 0;
                                  ros.angular['z'] = 0;
                                  var twist = {'linear': ros.linear, 'angular': ros.angular};
                                  ros.cmdVelMessage.publish(twist);
                                  setState(() {});
                              },
                              child: const Text(
                                "Brake",
                                style: TextStyle(
                                  fontSize: 35,
                                  color: Colors.white,
                                  ),
                              ),
                            );
                          }
                        ),
                      ),
                      Consumer<RosProvider>(
                        builder: (context, ros, child) {
                          return ArrowPad(
                            padding: const EdgeInsets.all(0),
                            height: height / 5,
                            width: width / 4,
                            iconColor: Colors.white,
                            innerColor: Colors.red,
                            outerColor: const Color(0xFFCC0000),
                            splashColor: const Color(0xFFCC0000),
                            hoverColor: const Color(0xFFFF4D4D),
                            onPressedUp: () {(double.parse(ros.linear['x']!.toStringAsFixed(2)));
                              if (((double.parse(ros.linear['x']!.toStringAsFixed(2))) == 0.4)){
                                ros.linear['x'] = 0.4;
                              } else {
                                ros.linear['x'] = ros.linear['x']! + 0.1;
                              }
                              if((ros.linear['x']! > -0.1) && (ros.linear['x']! < 0.1)) {
                                  ros.linear['x'] = 0;
                              }
                              var twist = {'linear': ros.linear, 'angular': ros.angular};
                              ros.cmdVelMessage.publish(twist);
                              setState(() {});
                            },
                            onPressedDown: () {
                              if (((double.parse(ros.linear['x']!.toStringAsFixed(2))) == -0.4)){
                                ros.linear['x'] = -0.4;
                              } else {
                                ros.linear['x'] = ros.linear['x']! - 0.1;
                              }
                              if((ros.linear['x']! > -0.1) && (ros.linear['x']! < 0.1)) {
                                  ros.linear['x'] = 0;
                              }
                              var twist = {'linear': ros.linear, 'angular': ros.angular};
                              ros.cmdVelMessage.publish(twist);
                              setState(() {});
                            },
                            onPressedLeft: () {
                              if (((double.parse(ros.angular['z']!.toStringAsFixed(2))) == 0.6)){
                                ros.angular['z'] = 0.6;
                              } else {
                                ros.angular['z'] = ros.angular['z']! + 0.2;
                              }
                              if((ros.angular['z']! > -0.1) && (ros.angular['z']! < 0.1)) {
                                  ros.angular['z'] = 0;
                              }
                              var twist = {'linear': ros.linear, 'angular': ros.angular};
                              ros.cmdVelMessage.publish(twist);
                              print(ros.angular['z']);
                              setState(() {});
                            },
                            onPressedRight: () {
                              if (((double.parse(ros.angular['z']!.toStringAsFixed(2)))  == -0.6)){
                                ros.angular['z'] = -0.6;
                              } else {
                                ros.angular['z'] = ros.angular['z']! - 0.2;
                              }
                              if((ros.angular['z']! > -0.1) && (ros.angular['z']! < -0.1)) {
                                  ros.angular['z'] = 0;
                                }
                              var twist = {'linear': ros.linear, 'angular': ros.angular};
                              ros.cmdVelMessage.publish(twist);
                              setState(() {});
                            },
                          );
                        }
                      ),
                      SizedBox( height: MediaQuery.of(context).size.height * 0.02),
                      Consumer<RosProvider>(
                        builder: (context, ros, child) {
                        return Text(
                          'F/B ${context.read<RosProvider>().linear['x']?.toStringAsFixed(1)} \nL/R ${context.read<RosProvider>().angular['z']?.toStringAsFixed(1)}',
                          textScaleFactor: 1.5,
                        );
                        }
                      ),
                      SizedBox(height: MediaQuery.of(context).size.height * 0.02),
                      Transform.scale(
                        scale: 0.75,
                        child: Consumer<RosProvider>(
                          builder: (context, ros, child) {
                            return ElevatedButton(
                              style: ButtonStyle(
                                shape: MaterialStateProperty.all<RoundedRectangleBorder>(
                                  RoundedRectangleBorder(
                                    borderRadius: BorderRadius.circular(18.0),
                                  )
                                ),
                                backgroundColor:  MaterialStateProperty.all<Color>( Colors.blue) // Example: Change background color when pressed
                              ),
                              onPressed: () {
                                Map<String, dynamic> json = {"data": "SaveMap"};
                                ros.commonMessage.publish(json);
                              },
                              child: const Text(
                                "SaveMap",
                                style: TextStyle(
                                  fontSize: 35,
                                  color: Colors.white,
                                  ),
                              ),
                            );
                          }
                        ),
                      ),
                    ],
                  ),
                ),
              ],
            ),
          ]
        ),
      ),
      drawer: const DrawerMenu(),
    );
  }
  // #enddocregion webview_widget
}