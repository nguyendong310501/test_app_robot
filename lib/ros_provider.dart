import 'package:flutter/material.dart';
import 'package:roslibdart/roslibdart.dart';
import 'dart:async';
import 'dart:convert';
import 'dart:typed_data';

class RosProvider extends ChangeNotifier{
  String host = 'ws://192.168.6.143:9090';
  //String host = 'ws://10.1.1.9:9090';
  // String host = 'ws://10.42.0.1:9090';

  late Ros _ros;
  late Topic _commonMessage;
  late Topic _cmdVelMessage;
  late Status _status;
  late Topic _cmdLintener;
  late Topic _imageLintener;

  Map<String, dynamic> _infoMsgReceived = {"data" : "0"};
  Uint8List? _imageData;

  late Map<String, double> _linear = {'x': 0.0, 'y': 0.0, 'z': 0.0};
  late Map<String, double>  _angular = {'x': 0.0, 'y': 0.0, 'z': 0.0};

  Ros get ros => _ros;
  Status get status => _status;

  Topic get commonMessage => _commonMessage;
  Topic get cmdVelMessage => _cmdVelMessage;

  Map<String, dynamic> get infoMsgReceived => _infoMsgReceived;
  Uint8List? get imageData => _imageData;

  Map<String, double> get linear => _linear;
  Map<String, double> get angular => _angular;

  void createRosTopic() async {
    _commonMessage = Topic(
        ros: _ros,
        name: '/appToRobot',
        type: "std_msgs/String",
        reconnectOnClose: false,
        queueLength: 100,
        queueSize: 100);
    _cmdVelMessage = Topic(
        ros: _ros,
        name: '/cmd_vel',
        type: "geometry_msgs/msg/Twist",
        reconnectOnClose: false,
        queueLength: 100,
        queueSize: 100);
    _cmdLintener = Topic(
      ros: _ros,
      name: '/robotToApp',
      type: "std_msgs/String",
      reconnectOnClose: true,
      queueLength: 100,
      queueSize: 100);
    _imageLintener = Topic(
      ros: _ros,
      name: '/image_topic',
      type: "std_msgs/String",
      reconnectOnClose: true,
      queueLength: 100,
      queueSize: 100);
}

  void initRos() async {
    _ros = Ros(url: host);
  }

  void connectRos() async {
    _ros.connect();
    Timer(const Duration(seconds: 3), () async {
      // reset subcriber and re-subcrive
      await _cmdLintener.unsubscribe();
      await _cmdLintener.subscribe(cmdListenerHandler);
      await _imageLintener.unsubscribe();
      await _imageLintener.subscribe(imageListenerHandler);
      //print("subcribe ===================");
      // await chatter.subscribe();
    });
  }


  void destroyRos() async {
    await _ros.close();
  }

  Future<void> imageListenerHandler(Map<String, dynamic> msg) async {
    var rawDataString = msg["data"]; // uint8 string (bgr8)
    List<int> bytes = base64.decode(rawDataString); // convert base64 data to bytes
    _imageData = Uint8List.fromList(bytes);
    print("2====================");
    print("$msg");
    notifyListeners();
  }

  Future<void> cmdListenerHandler(Map<String, dynamic> msg) async {
    _infoMsgReceived = msg;
    //print("$_msgReceived ===================");
    notifyListeners();
  }

  set linear(newValue) {
    _linear = newValue;
    notifyListeners();
  }

  set angular(newValue) {
    _angular = newValue;
    notifyListeners();
  }
}