import 'package:shared_preferences/shared_preferences.dart';

class SharedPrefs {
  late SharedPreferences _prefs;

  Future initialise() async {
    _prefs = await SharedPreferences.getInstance();
  }

  set tableNumbers(List<String>? value) {
    _prefs.setStringList('table_number', value ?? []);
  }

  List<String>? get tableNumbers{
    if (!_prefs.containsKey('table_number')) {
      return null;
    }

    return _prefs.getStringList('table_number');
  }

  set numbers(int? value) {
    _prefs.setInt('number', value ?? 0);
  }

  int? get numbers{
    if (!_prefs.containsKey('number')) {
      return null;
    }

    return _prefs.getInt('number');
  }

  void removeTableNumbers() {
    _prefs.remove('table_number');
  }
}