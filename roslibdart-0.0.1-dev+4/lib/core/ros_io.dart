import 'package:web_socket_channel/web_socket_channel.dart';
import 'package:web_socket_channel/io.dart';

WebSocketChannel initializeWebSocketChannel(String url) {
  return IOWebSocketChannel.connect(url, connectTimeout: Duration(seconds: 2), pingInterval: Duration(seconds: 10));
  //return IOWebSocketChannel.connect(url, connectTimeout: Duration(seconds: 2));
  //return IOWebSocketChannel.connect(url); //marked
}
